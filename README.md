# cornac - Enterprise-class Postgres deployment

The project is subdivided as following:

- `appliance/` is an Ansible project to maintain Postgres host.
- `service/` is a Python project implementing RDS-compatible webservice.
